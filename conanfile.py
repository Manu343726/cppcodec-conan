from conans import ConanFile
import os

class CppcodecConan(ConanFile):
    name = 'cppcodec'
    url = 'https://gitlab.com/Manu343726/cppcodec-conan'
    description = 'Header-only C++11 library to encode/decode base64, base64url, base32, base32hex and hex (a.k.a. base16) as specified in RFC 4648, plus Crockford\'s base32. MIT licensed with consistent, flexible API.'
    version = '0.2'
    scm = {
        'type': 'git',
        'url': 'https://github.com/tplgy/cppcodec',
        'revision': 'v' + version
    }

    def package(self):
        self.copy('*.hpp', src='cppcodec', dst=os.path.join('include', 'cppcodec'))

    def package_id(self):
        self.info.header_only()
